import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean lanjut=true;
        while (lanjut) {

            System.out.print("Masukkan NIM mahasiswa: ");
            String nim = scanner.nextLine();
            System.out.print("Masukkan nama mahasiswa: ");
            String nama = scanner.nextLine();

            Mahasiswa mahasiswa = new Mahasiswa(nim, nama);
            System.out.println();
            System.out.println("Lengkapi berkas berikut...");
            boolean oke = true;
            while (oke) {
                System.out.print("Kode mata kuliah: ");
                String kode = scanner.nextLine();

                System.out.print("Nama mata kuliah: ");
                String namaMK = scanner.nextLine();

                System.out.print("Nilai: ");
                int nilai = scanner.nextInt();
                scanner.nextLine();

                System.out.println();
                mahasiswa.tambahMataKuliah(new MataKuliah(kode, namaMK, nilai));
                System.out.println("Input Mata Kuliah lain? (y/n)");
                String next = scanner.next();
                scanner.nextLine();
                if (next.equalsIgnoreCase("n")) oke = false;
            }

            mahasiswa.cetakKHS();
            System.out.println("Cetak KHS lain? (y/n)");
            String next = scanner.next();
            scanner.nextLine();
            if (next.equalsIgnoreCase("n")) lanjut = false;
        }
        System.out.println("Aplikasi selesai...");
    }
}
