import java.util.ArrayList;

public class Mahasiswa {
    private String nim;
    private String nama;
    private ArrayList<MataKuliah> listMatkul;

    public Mahasiswa(String nim, String nama) {
        this.nim = nim;
        this.nama = nama;
        this.listMatkul=new ArrayList<>();
    }

    public void tambahMataKuliah(MataKuliah mataKuliah) {
        listMatkul.add(mataKuliah);
    }

    public void cetakKHS() {
        System.out.println("NIM\t\t\t: " + nim);
        System.out.println("Nama\t\t: " + nama);
        System.out.println();
        System.out.println("       Kartu Hasil Studi (KHS)");
        System.out.printf("=======================================\n");
        System.out.printf("|%-10s|%-20s|%-5s|\n","Kode","Nama Mata Kuliah","Nilai");
        System.out.printf("=======================================\n");
        for (MataKuliah mataKuliah : listMatkul) {
            System.out.printf("|%-10s|%-20s|%-5s|\n",mataKuliah.getKode(),mataKuliah.getNamaMatkul(),mataKuliah.getNilaiHuruf());
        }
    }
}
