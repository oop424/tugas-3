public class MataKuliah {
    private String kode;
    private String nama;
    private int nilaiAngka;

    public MataKuliah(String kode, String nama, int nilaiAngka) {
        this.kode = kode;
        this.nama = nama;
        this.nilaiAngka = nilaiAngka;
    }

    public String getKode() {
        return kode;
    }

    public String getNamaMatkul() {
        return nama;
    }

    public String getNilaiHuruf() {
        if (nilaiAngka >= 80) {
            return "A";
        } else if (nilaiAngka >= 70) {
            return "B";
        } else if (nilaiAngka >= 60) {
            return "C";
        } else if (nilaiAngka >= 50) {
            return "D";
        } else {
            return "E";
        }
    }
}
